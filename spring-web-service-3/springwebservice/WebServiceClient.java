package com.joxemon.labs.spring.boot.springwebservice;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import com.joxemon.labs.spring.boot.springwebservice.wsdl.AddBook;
import com.joxemon.labs.spring.boot.springwebservice.wsdl.AddBookResponse;

public class WebServiceClient extends WebServiceGatewaySupport {
	private static final Logger log = LoggerFactory.getLogger(WebServiceClient.class);
	
	public AddBookResponse addBookResponse(AddBook addBook) {
		AddBookResponse abr = (AddBookResponse) getWebServiceTemplate()
				.marshalSendAndReceive("http://localhost:8088/mockBookServiceSOAP", addBook,
	            new SoapActionCallback(
	                    "http://www.cleverbuilder.com/BookService"));
		
		
		return abr;
	}

}
