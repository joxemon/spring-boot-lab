package example.webClient.config;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import example.webClient.config.wsdl.AddBook;
import example.webClient.config.wsdl.AddBookResponse;

public class WebServiceClient extends WebServiceGatewaySupport {
	private static final Logger log = LoggerFactory.getLogger(WebServiceClient.class);
	
	public AddBookResponse addBookResponse(AddBook addBook) {
		AddBookResponse abr = (AddBookResponse) getWebServiceTemplate()
				.marshalSendAndReceive("http://localhost:8088/mockBookServiceSOAP", addBook,
	            new SoapActionCallback(
	                    "http://www.cleverbuilder.com/BookService"));
		
		
		return abr;
	}

}
