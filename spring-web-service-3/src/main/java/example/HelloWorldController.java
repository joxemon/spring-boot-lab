package example;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import example.webClient.config.WebServiceClient;
import example.webClient.config.wsdl.AddBook;
import example.webClient.config.wsdl.AddBookResponse;

@Controller
public class HelloWorldController {
	
	@Autowired
	WebServiceClient wsc;
	
	  private static final String template = "Hello, %s!";
	  private final AtomicLong counter = new AtomicLong();

	  @GetMapping("/hello-world")
	  @ResponseBody
	  public AddBookResponse sayHello(@RequestParam(name="name", required=false, defaultValue="Stranger") String name) {
		  
		  return wsc.addBookResponse(new AddBook());

	  }
	  

	  
}
