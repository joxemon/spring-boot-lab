package example;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringWebService3Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringWebService3Application.class, args);
	}

}
