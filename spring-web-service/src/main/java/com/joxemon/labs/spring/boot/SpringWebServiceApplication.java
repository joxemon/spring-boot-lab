package com.joxemon.labs.spring.boot;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import com.joxemon.labs.spring.boot.springwebservice.WebServiceClient;
import com.joxemon.labs.spring.boot.springwebservice.wsdl.AddBook;
import com.joxemon.labs.spring.boot.springwebservice.wsdl.AddBookResponse;

@SpringBootApplication
public class SpringWebServiceApplication {

	public static void main(String[] args) {

		SpringApplication.run(SpringWebServiceApplication.class, args);

	}
	
	@Bean
	  CommandLineRunner lookup(WebServiceClient quoteClient) {
		
	    return args -> {
	      AddBook addBook = new AddBook();
	      AddBookResponse response = quoteClient.addBookResponse(addBook);
	      System.err.println(response.getID());
	      
	    };

	  }

}
