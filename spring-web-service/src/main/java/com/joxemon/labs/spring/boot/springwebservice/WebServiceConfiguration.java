package com.joxemon.labs.spring.boot.springwebservice;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

@Configuration
public class WebServiceConfiguration {

	
	 @Bean
	  public Jaxb2Marshaller marshaller() {
	    Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
	    // this package must match the package in the <generatePackage> specified in
	    // pom.xml
	    marshaller.setContextPath("com.joxemon.labs.spring.boot.springwebservice.wsdl");
	    return marshaller;
	  }

	  @Bean
	  public WebServiceClient countryClient(Jaxb2Marshaller marshaller) {
		  WebServiceClient client = new WebServiceClient();
	    client.setDefaultUri("http://localhost:8088");
	    client.setMarshaller(marshaller);
	    client.setUnmarshaller(marshaller);
	    return client;
	  }
}
