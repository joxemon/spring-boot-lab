package example.webClient.service;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import example.webClient.msg.AddBook;
import example.webClient.msg.AddBookResponse;

public class WebServiceClient extends WebServiceGatewaySupport {
	private static final Logger log = LoggerFactory.getLogger(WebServiceClient.class);
	
	//@Cacheable(value="cache", keyGenerator="customKeyGenerator", unless = "#addBook.author=='tom'")
	//@Cacheable(value="cache", keyGenerator="customKeyGenerator", unless = "#root.key!='NN'")
	@Cacheable(value="cache", keyGenerator="customKeyGenerator", condition = "condition")
	public AddBookResponse addBookResponse(AddBook addBook) {
		AddBookResponse abr = (AddBookResponse) getWebServiceTemplate()
				.marshalSendAndReceive("http://localhost:8088/mockBookServiceSOAP", addBook,
	            new SoapActionCallback(
	                    "http://www.cleverbuilder.com/BookService"));
		
		
		return abr;
	}
	@Caching(cacheable= {
	@Cacheable(value="cache", keyGenerator="customKeyGenerator", condition="#addBook.title=='NO' || #addBook.author==null"),
	@Cacheable(value="cache", keyGenerator="customKeyGenerator", condition="#addBook.isValid() && #addBook.listLenght()>4"),
	@Cacheable(value="cache", keyGenerator="customKeyGenerator", condition="#addBook.getList()[1].getID()=='aab'")}
	)
	//@Caching(evict = { @CacheEvict("primary"), @CacheEvict(value = "secondary", key = "#p0") })
	public AddBookResponse addBookResponse1(AddBook addBook) {
		AddBookResponse abr = (AddBookResponse) getWebServiceTemplate()
				.marshalSendAndReceive("http://localhost:8088/mockBookServiceSOAP", addBook,
	            new SoapActionCallback(
	                    "http://www.cleverbuilder.com/BookService"));
		
		
		return abr;
	}
	
    @CacheEvict(allEntries = true, cacheNames = "cache")
    @Scheduled(fixedDelayString = "${cache.time.flush}" )
    public void reportCacheEvict() {
        System.out.println("Flush Cache " );
    }


}
