package example.facade;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import example.webClient.msg.AddBook;
import example.webClient.msg.AddBookResponse;
import example.webClient.service.WebServiceClient;

@Controller
public class HelloWorldController {
	
	@Autowired
	WebServiceClient wsc;
	
	  private static final String template = "Hello, %s!";
	  private final AtomicLong counter = new AtomicLong();

	  @GetMapping("/hello-world")
	  @ResponseBody
	  
	  public AddBookResponse addBook(@RequestParam(name="title") String title,@RequestParam(name="author") String author,@RequestParam(name="other") String other) {
		  
		  AddBook ab = new AddBook();
		  ab.setAuthor(author);
		  ab.setTitle(title);
		  ab.setOther(other);
		  
		  return wsc.addBookResponse(ab);

	  }
	  
	  @GetMapping("/test-cache")
	  @ResponseBody
	  
	  public AddBookResponse test_cache(@RequestParam(name="title") String title,@RequestParam(name="author") String author,@RequestParam(name="other") String other) {
		  
		  AddBook ab = new AddBook();
		  if(author=="null") {
			  ab.setAuthor(null);
		  }else {
			  ab.setAuthor("author");
		  }
		  
		  ab.setTitle(title);
		  ab.setOther("other");
		  
		  return wsc.addBookResponse1(ab);

	  }
	  

	  
}
