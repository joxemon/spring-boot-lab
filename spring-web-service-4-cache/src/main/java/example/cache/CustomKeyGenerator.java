package example.cache;

import java.lang.reflect.Method;

import org.springframework.cache.interceptor.KeyGenerator;

import example.webClient.msg.AddBook;

public class CustomKeyGenerator implements KeyGenerator {
	private String cache = "";
	@Override
	public Object generate(Object target, Method method, Object... params) {
		
		AddBook ab = (AddBook) params[0];
		System.out.println(ab.toString());
		this.cache = ab.getAuthor();
		return cache;
	}
	
	public String getCache() {
		return cache;
	}

	
}
