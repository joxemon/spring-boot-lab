package example.cache;

import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachingConfigurerSupport;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.concurrent.ConcurrentMapCacheManager;
import org.springframework.cache.interceptor.KeyGenerator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.expression.Expression;
import org.springframework.scheduling.annotation.EnableScheduling;

@Configuration
@EnableCaching
@EnableScheduling
public class CachingConfig extends CachingConfigurerSupport{
 
    @Bean
    public CacheManager cacheManager() {
    	ConcurrentMapCacheManager cmcm = new ConcurrentMapCacheManager("cache");
    	//cmcm.setAllowNullValues(true);
        return cmcm;
    }
    
    @Bean("customKeyGenerator")
    public KeyGenerator keyGenerator() {
        return new CustomKeyGenerator();
    }
    
    @Bean("customExpression")
    public Expression expression() {
        return new ExpressionGenerator();
    }

    
}
