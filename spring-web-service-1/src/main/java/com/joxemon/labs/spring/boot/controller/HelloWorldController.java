package com.joxemon.labs.spring.boot.controller;

import java.util.concurrent.atomic.AtomicLong;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.joxemon.labs.spring.boot.dto.Entrada;

@Controller
public class HelloWorldController {

	
	  private static final String template = "Hello, %s!";
	  private final AtomicLong counter = new AtomicLong();

	  @GetMapping("/hello-world")
	  @ResponseBody
	  public Entrada sayHello(@RequestParam(name="name", required=false, defaultValue="Stranger") String name) {
	    return new Entrada(counter.incrementAndGet(), String.format(template, name));
	  }
	  
}
